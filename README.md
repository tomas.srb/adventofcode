Scripty původně nebyly určeny ke zveřejnění a pokud někoho uráží, tak se omlouvám. Obsahují spoustu nadbytečného kódu z neúspěšných pokusů a zbytečné ladící výpisy, neobsahují zadání ani žádné vysvětlující komentáře.

Jde o řešení úloh z https://adventofcode.com/2020, komentovaná řešení jiného řešitele jsou na https://www.abclinuxu.cz/blog/GretinBlogisek/2020/12/pocity-zazitky-a-dojmy-z-adventofcode-2020

#!/usr/bin/env python3

data = open('input').read().split()

bin_data = set((int(seat.replace('F', '0').replace('B', '1').replace('R', '1').replace('L', '0'), 2) for seat in data))

print(max(bin_data))

for seat in range(min(bin_data), max(bin_data)):
    if seat not in bin_data:
        print(seat)

#!/usr/bin/env python3

input = '137826495'
test = '389125467'

cups = tuple(int(ch) for ch in input)
#cups = tuple(int(ch) for ch in test)

cups = cups + tuple(range(max(cups)+1, 1000001))

def turn(cups):
    picked = cups[1:4]
    cups = cups[0:1] + cups[4:]
    dest_cup = cups[0] - 1
    while dest_cup not in cups:
        dest_cup -= 1
        if dest_cup < 1:
            dest_cup = max(cups)
    dest_index = cups.index(dest_cup)
    cups = cups[:dest_index + 1] + picked + cups[dest_index + 1:]
    cups = cups[1:] + cups[0:1]
    return cups

#for i in range(10000000):
for i in range(200):
    print(i, end='\r')
    cups = turn(cups)

print()
index_1 = cups.index(1)
cups = cups[index_1:] + cups[:index_1]
print(cups[1], '*', cups[2], '=', cups[1] * cups[2])

Uses sysutils;

type
  PCup = ^TCup;
  TCup = record
           lab: longint;
           next: PCup;
         end;
  TMap = array [1..1000000] of PCup;

const
  input_prod = '137826495';
  input_test = '389125467';

function Turn(CurrentCup: PCup; var Map: TMap): PCup;
  var
    Picked: PCup;
    DestCupLab: LongInt;
    DestCup: PCup;
  begin
    Picked := CurrentCup^.next;
    CurrentCup^.next := Picked^.next^.next^.next;
    DestCupLab := CurrentCup^.lab - 1;
    while (DestCupLab = Picked^.lab) or
          (DestCupLab = Picked^.next^.lab) or
          (DestCupLab = Picked^.next^.next^.lab) do Dec(DestCupLab);
    if DestCupLab < 1 then
    begin
      DestCupLab := length(Map);
      while (DestCupLab = Picked^.lab) or
            (DestCupLab = Picked^.next^.lab) or
            (DestCupLab = Picked^.next^.next^.lab) do Dec(DestCupLab);
    end;
    DestCup := Map[DestCupLab];
    Picked^.next^.next^.next := DestCup^.next;
    DestCup^.next := Picked;
    Turn := CurrentCup^.next;
  end;

function CreateCups(init: String; var Map: TMap): PCup;
  var
    start: PCup;
    last: PCup;
    I: LongInt;
    Max: LongInt;
  begin
    start := New(PCup);
    start^.lab := StrToInt(init[1]);
    Max := start^.lab;
    last := start;
    for I := 2 to length(init) do
    begin
      last^.next := New(PCup);
      last := last^.next;
      last^.lab := StrToInt(init[I]);
      if last^.lab > Max then Max := last^.lab;
    end;
    WriteLn(Max);
    for I := Max + 1 to 1000000 do
    begin
      last^.next := New(PCup);
      last := last^.next;
      last^.lab := I;
    end;
    last^.next := start;
    last := start;
    repeat
      Map[last^.lab] := last;
      last := last^.next;
    until last = start;
    CreateCups := start;
  end;


procedure PrintCups(CurrentCup: PCup);
  var
    Point: PCup;
  begin
    Point := CurrentCup;
    repeat
      Write(Point^.lab, ' ');
      Point :=Point^.next;
    until Point = CurrentCup;
    WriteLn();
  end;

var
  current_cup: PCup;
  I: Longint;
  Map: TMap;
begin
  current_cup := CreateCups(input_prod, Map);
  for I := 1 to 10000000 do
  begin
    if I and $FFFF = 0 then Write(I, ^M);
    current_cup := Turn(current_cup, Map);
  end;
  WriteLn(10000000);
  while current_cup^.lab <> 1 do
  begin
    current_cup := current_cup^.next;
  end;
  WriteLn(current_cup^.next^.lab, ' * ', current_cup^.next^.next^.lab, ' = ', current_cup^.next^.lab * current_cup^.next^.next^.lab);
end.


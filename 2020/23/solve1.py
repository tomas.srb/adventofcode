#!/usr/bin/env python3

input = '137826495'
test = '389125467'

cups = tuple(int(ch) for ch in input)
cups = tuple(int(ch) for ch in test)


def turn(cups):
    picked = cups[1:4]
    print(picked)
    cups = cups[0:1] + cups[4:]
    print(cups)
    dest_cup = cups[0] - 1
    while dest_cup not in cups:
        dest_cup -= 1
        if dest_cup < 1:
            dest_cup = max(cups)
        print(dest_cup)
    dest_index = cups.index(dest_cup)
    cups = cups[:dest_index + 1] + picked + cups[dest_index + 1:]
    cups = cups[1:] + cups[0:1]
    return cups

for i in range(100):
    cups = turn(cups)
    print(cups)

index_1 = cups.index(1)
print(''.join(str(cup) for cup in (cups[index_1:] + cups[:index_1]))[1:])

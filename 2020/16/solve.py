#!/usr/bin/env python3

import re

filename = 'input'
#filename = 'test2'
rules_src, my_ticket_src, tickets_src = [item.split('\n') for item in open(filename).read().split('\n\n')]

r_rule = re.compile(r'^(?P<name>.+): (?P<lo1>\d+)-(?P<hi1>\d+) or (?P<lo2>\d+)-(?P<hi2>\d+)$')

rules = [r_rule.match(line).groupdict() for line in rules_src]
for rule in rules:
    rule['lo1'] = int(rule['lo1'])
    rule['hi1'] = int(rule['hi1'])
    rule['lo2'] = int(rule['lo2'])
    rule['hi2'] = int(rule['hi2'])

tickets = [list(map(int, line.split(','))) for line in tickets_src[1:] if line]

my_ticket = [int(item) for item in my_ticket_src[1].split(',')]

def match_rule(rule, value):
    if value >= rule['lo1'] and value <= rule['hi1']:
        return True
    if value >= rule['lo2'] and value <= rule['hi2']:
        return True
    return False

def match_any_rule(rules, value):
    for rule in rules:
        if value >= rule['lo1'] and value <= rule['hi1']:
            return True
        if value >= rule['lo2'] and value <= rule['hi2']:
            return True
    return False

error_rate = 0

valid_tickets = [my_ticket]
for ticket in tickets:
    valid = True
    for value in ticket:
        if not match_any_rule(rules, value):
            error_rate += value
            valid = False
    if valid:
        valid_tickets.append(ticket)
print(error_rate)

rule_names = {rule['name'] for rule in rules}
column_names = [rule_names for column in my_ticket]
rule_by_name = {rule['name']: rule for rule in rules}

for ticket in valid_tickets:
    for column, value in enumerate(ticket):
        names = column_names[column]
        for name in names:
            if not match_rule(rule_by_name[name], value):
                column_names[column] = column_names[column] - set([name])

eliminate = True
available_names = rule_names
solved_columns = set()
while eliminate:
    eliminate = False
    for column, names in enumerate(column_names):
        if column not in solved_columns:
            names = {name for name in names if name in available_names}
            if len(names) == 1:
                available_names = available_names - names
                solved_columns.add(column)
            if len(names) != len(column_names[column]):
                eliminate = True
                column_names[column] = names

print(column_names)

departure = 1
for column, names in enumerate(column_names):
    name = names.pop()
    if name.startswith('departure'):
        departure *= my_ticket[column]
print(departure)

#!/usr/bin/env python3

import re

filename = 'input'
filename = 'test'
rules_src, my_ticket_src, tickets_src = [item.split('\n') for item in open(filename).read().split('\n\n')]

r_rule = re.compile(r'^(?P<name>.+): (?P<lo1>\d+)-(?P<hi1>\d+) or (?P<lo2>\d+)-(?P<hi2>\d+)$')

rules = [r_rule.match(line).groupdict() for line in rules_src]
for rule in rules:
    rule['lo1'] = int(rule['lo1'])
    rule['hi1'] = int(rule['hi1'])
    rule['lo2'] = int(rule['lo2'])
    rule['hi2'] = int(rule['hi2'])

tickets = [list(map(int, line.split(','))) for line in tickets_src[1:] if line]

my_ticket = [int(item) for item in my_ticket_src[1].split(',')]

def match_any_rule(rules, value):
    for rule in rules:
        if value >= rule['lo1'] and value <= rule['hi1']:
            return True
        if value >= rule['lo2'] and value <= rule['hi2']:
            return True
    return False

error_rate = 0

valid_tickets = [my_ticket]
for ticket in tickets:
    valid = True
    for value in ticket:
        if not match_any_rule(rules, value):
            error_rate += value
            valid = False
    if valid:
        valid_tickets.append(ticket)
print(error_rate)
print(valid_tickets)

#!/usr/bin/env python3

import re

r_pass = re.compile(r'^(?P<lo>\d+)-(?P<hi>\d+) (?P<ch>\w): (?P<text>\w+)\n?$')

good = 0

good_2 = 0

for line in open('input'):
    m_pass = r_pass.match(line)
    if m_pass:
        lo = int(m_pass.group('lo'))
        hi = int(m_pass.group('hi'))
        ch = m_pass.group('ch')
        text = m_pass.group('text')
        count = len([c for c in text if c == ch])
        if count < lo or count > hi:
            print(line.strip(), count)
        else:
            good += 1
        if (text[lo-1] == ch and text[hi-1] != ch) or (text[lo-1] != ch and text[hi-1] == ch):
            print(line.strip(), text[lo-1], text[hi-1])
            good_2 += 1
    else:
        print(f'"{line}"')

print(good)
print(good_2)

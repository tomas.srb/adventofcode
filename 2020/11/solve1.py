#!/usr/bin/env python3

inp = 'input'
#inp = 'test'

data = [list(line.strip()) for line in open(inp).readlines()]

ROWS = len(data)
COLUMNS = len(data[0])
CHANGES = False

def count_occupied(data):
    count = 0
    for row in data:
        for column in row:
            if column == '#':
                count += 1
    return count

def print_data(data):
  print('\n' + '\n'.join([''.join(line) for line in data]))
  print('Occupied:', count_occupied(data))

def get_seat(data, row, column):
    if row < 0 or row >= ROWS or column < 0 or column >= COLUMNS:
        return ','
    return data[row][column]

adjancents = (
    (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1)
)

def four_occupied(data, row, column):
    count = 0 
    for dr, dc in adjancents:
        if get_seat(data, row + dr, column + dc) == '#':
            count += 1
            if count >= 4:
                return True
    return False

def no_occupied(data, row, column):
    for dr, dc in adjancents:
        if get_seat(data, row + dr, column + dc) == '#':
            return False
    return True

def solve_seat(data, row, column):
    global CHANGES
    seat = get_seat(data, row, column)
    if seat == '#' and four_occupied(data, row, column):
        CHANGES = True
        return 'L'
    if seat == 'L' and no_occupied(data, row, column):
        CHANGES = True
        return '#'
    return seat

def solve_board(last):
    global CHANGES
    CHANGES = False
    new = []
    for row in range(0, ROWS):
        new_row = []
        new.append(new_row)
        for column in range(0, COLUMNS):
            new_row.append(solve_seat(last, row, column))
    return new

print_data(data)
new = solve_board(data)
for i in range(0, 10000):
    print(i, end='\r')
    new = solve_board(new)
    if not CHANGES:
        break

print_data(new)

#!/usr/bin/env python3

input = '8,11,0,19,1,2'
#input = '0,3,6'

numbers = [int(item) for item in input.split(',')]
seen = {number: index + 1 for index, number in enumerate(numbers[:-1])}
start_turn = len(numbers) + 1
last = numbers[-1]

for turn in range(start_turn, 30000000+1):
    if last in seen:
        current = turn - seen[last] - 1
    else:
        current = 0
    seen[last] = turn - 1
    last = current
#    print(turn, current, seen)
    print('{:9} {:9} {:9}'.format(turn, current, len(seen)), end='      \r')

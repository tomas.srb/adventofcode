#!/usr/bin/env python3

data = open('input').readlines()
#data = open('test').readlines()

board = set()

step = {
    'ne': (1, 1),
    'e': (2, 0),
    'se': (1, -1),
    'sw': (-1, -1),
    'w': (-2, 0),
    'nw': (-1, 1)
}

def go_step(x, y, direction):
  return step[direction][0] + x, step[direction][1] + y

for path in data:
    lastch = ''
    x, y = 0, 0
    for ch in path.strip():
        if lastch + ch in step:
            x, y = go_step(x, y, lastch + ch)
            lastch = ''
        elif lastch:
          raise Exception('Unknown direction %s', lastch)
        else:
            lastch = ch
    if lastch:
        raise Exception('Unknown direction %s', lastch)
    if (x, y) in board:
        print(f'Flip to white {x}, {y}')
        board.remove((x, y))
    else:
        print(f'Flip to black {x}, {y}')
        board.add((x, y))

print(len(board))

def black_neighs(board, x, y):
  return sum(1 for dx, dy in step.values() if (x + dx, y + dy) in board)

def black_to_white(board, x, y, new_board):
    if black_neighs(board, x, y) in (1, 2):
        new_board.add((x, y))

def white_to_black(board, x, y, new_board):
    if black_neighs(board, x, y) == 2:
        new_board.add((x, y))

def process(board):
    new_board = set()
    for x, y in board:
        black_to_white(board, x, y, new_board)
        for dx, dy in step.values():
            white_to_black(board, x + dx, y + dy, new_board)
    return new_board

for _ in range(100):
    board = process(board)

print(len(board))

#!/usr/bin/env python3

filename = 'input'
#filename = 'test1'
#filename = 'cyklus'

players_str = open(filename).read().split('\n\n')

players = [
  tuple(tuple(int(card) for card in player_str.strip().split('\n')[1:])) for player_str in players_str
]

def turn(players):
    if len(players[0]) > players[0][0] and len(players[1]) > players[1][0]:
        return game((players[0][1:players[0][0]+1], players[1][1:players[1][0]+1]))
    if players[0][0] > players[1][0]:
        return 0
    if players[0][0] < players[1][0]:
        return 1
    Exception('Nerozhodne %s, %s', players[0] > players[1])

def game(players):
    rounds = set()
    while players[0] and players[1]:
        res = turn(players)
        if res == 0:
            players = (
              players[0][1:] + (players[0][0], players[1][0]),
              players[1][1:]
            )
        elif res == 1:
            players = (
              players[0][1:],
              players[1][1:] + (players[1][0], players[0][0])
            )
        if players in rounds:
            print('Cyklus', players)
            return 0
        rounds.add(players)
    if players[0]:
        print(score(players[0]))
        return 0
    else:
        print(score(players[1]))
        return 1

def score(player):
    return sum(sc * card for sc, card in enumerate(player[::-1], 1))

game(players)

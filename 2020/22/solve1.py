#!/usr/bin/env python3

filename = 'input'
#filename = 'test1'

players_str = open(filename).read().split('\n\n')

players = [
  [int(card) for card in player_str.strip().split('\n')[1:]] for player_str in players_str
]

def turn(players):
    if players[0][0] > players[1][0]:
        return [
          players[0][1:] + [players[0][0], players[1][0]],
          players[1][1:]
        ]
    if players[0][0] < players[1][0]:
        return [
          players[0][1:],
          players[1][1:] + [players[1][0], players[0][0]]
        ]
    Exception('Nerozhodne %s, %s', players[0] > players[1])

def score(player):
    return sum(sc * card for sc, card in enumerate(player[::-1], 1))

print(players)
while players[0] and players[1]:
    players = turn(players)
    print(players)

print(score(players[0]), score(players[1]))

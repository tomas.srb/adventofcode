#!/usr/bin/env python3

import re
import collections

r_rule = re.compile(r'^(?P<odstin>\w+) (?P<barva>\w+) bags contain(?P<obsah> (?P<vpocet>\d+) (?P<vodstin>\w+) (?P<vbarva>\w+) bags?[,.]| no other bags.)+$')

rules = dict()

def get_bags(parts):
    out = []
    i = 4
    while i < len(parts):
        if ' '.join(parts[i:i+2]) != 'no other':
           out.append((int(parts[i]), ' '.join(parts[i+1:i+3])))
        i += 4
    return out

for line in open('input'):
    m_rule = r_rule.match(line.strip())
    if m_rule:
        parts = line.strip().replace(',', '').split(' ')
        rules[' '.join(parts[0:2])] = get_bags(parts)
    else:
        print(line)

reverted_rules = collections.defaultdict(list)
for rule, inside in rules.items():
    for count, color in inside:
        reverted_rules[color].append(rule)

def search_bags(color, known_bags):
    if color in known_bags:
        return
    known_bags.add(color)
    for container in reverted_rules[color]:
        search_bags(container, known_bags)

bags = set()
search_bags('shiny gold', bags)
print(bags, len(bags) - 1)

def search_bags_inside(color, count):
    total = count
    for contain_count, container in rules[color]:
        total += count * search_bags_inside(container, contain_count)
    return total

total = search_bags_inside('shiny gold', 1)
print(total - 1)

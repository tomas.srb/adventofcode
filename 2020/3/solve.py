#!/usr/bin/env python3

def slope(right, down):
    pos_x = 0
    pos_y = down
    trees = 0
    for line in open('input'):
        text = list(line.strip())
        if pos_y < down:
            print(''.join(text))
            pos_y += 1
            continue
        if pos_x >= len(text):
            pos_x -= len(text)
        if text[pos_x] == '#':
            trees += 1
            text[pos_x] = 'X'
        else:
            text[pos_x] = 'O'
        print(''.join(text))
        pos_x += right
        pos_y = 1
    return trees

trees = [slope(r, d) for r, d in ((1,1), (3,1), (5,1), (7, 1), (1,2))]

total = 1
for tree in trees:
    total = total * tree
print(total)



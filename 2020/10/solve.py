#!/usr/bin/env python3

import collections
inp = [int(line) for line in open('input').readlines()]
inp = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]
inp = [28 ,33 ,18 ,42 ,31 ,14 ,46 ,20 ,48 ,47 ,24 ,23 ,49 ,45 ,19 ,38 ,39 ,11 ,1 ,32 ,25 ,35 ,8 ,17 ,7 ,9 ,4 ,2 ,34 ,10 ,3]
inp.append(0)
data = sorted(inp)
data.append(data[-1] + 3)

print(data)
diffs = {1:0, 3:0}

for i in range(1, len(data)):
    diff = data[i]-data[i-1]
    print(diff)
    diffs[diff] += 1

print(diffs, diffs[1] * diffs[3])

count = 0
data_count = len(data)

def try_path(pos):
  global count, data_count
  if pos == len(data) - 1:
      count += 1
      print(count, end='\r')
      return None
  new_pos = pos + 1
  while new_pos < data_count and data[new_pos] - data[pos] <= 3:
      try_path(new_pos)
      new_pos += 1
try_path(0)

print('\n', count)

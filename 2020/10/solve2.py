#!/usr/bin/env python3

inp = [int(line) for line in open('input').readlines()]
#inp = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]

#inp = [28 ,33 ,18 ,42 ,31 ,14 ,46 ,20 ,48 ,47 ,24 ,23 ,49 ,45 ,19 ,38 ,39 ,11 ,1 ,32 ,25 ,35 ,8 ,17 ,7 ,9 ,4 ,2 ,34 ,10 ,3]
inp.append(0)
data = sorted(inp)
data.append(data[-1] + 3)
print(data)

def try_path(data, pos):
    data_count = len(data)
    count = 0
    if pos == data_count - 1:
        print(count, end='\r')
        return 1
    new_pos = pos + 1
    while new_pos < data_count and data[new_pos] - data[pos] <= 3:
        count += try_path(data, new_pos)
        new_pos += 1
    return count

def solve_range(data):
    return try_path(data, 0)

pos = 0
end = 10
last_count = 0
data_count = len(data)
count = 1
while pos < data_count:
    while True:
        curr_count = solve_range(data[pos:end])
        if curr_count == last_count:
            print(pos, end, curr_count)
            count *= curr_count
            pos = end - 3
            end += 16
            last_count = 0
            break
        last_count = curr_count
        end += 3

print(count)


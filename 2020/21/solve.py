#!/usr/bin/env python3

from collections import defaultdict

filename = 'input'
#filename = 'test'

class Food:
    def __init__(self, ingreds, alergens):
        self.ingreds = ingreds
        self.alergens = alergens

class Menu:
    def __init__(self):
        self.foods = []
        self.ingreds = set()
        self.alergens = defaultdict(list)

    def append(self, food):
        self.foods.append(food)
        for alergen in food.alergens:
            self.alergens[alergen].append(food.ingreds)
            self.ingreds.update(food.ingreds)

    def intersections(self):
        known_ingreds_by_alergen = dict()
        any_to_remove = True
        while any_to_remove:
            any_to_remove = False
            for alergen, ingreds in self.alergens.items():
                if ingreds:
                    ingreds_all = set.intersection(*ingreds)
                    if len(ingreds_all) == 1:
                        known_ingreds_by_alergen[alergen] = tuple(ingreds_all)[0]
            for alergen, ingreds in self.alergens.items():
                for alergen, known_ingred in known_ingreds_by_alergen.items():
                    for ingred in ingreds:
                        if known_ingred in ingred:
                            ingred.remove(known_ingred)
                            any_to_remove = True
        return known_ingreds_by_alergen

menu = Menu()
for line in open(filename):
    ingreds_str, alergens_str = line.strip().rstrip(')').split(' (contains ')
    ingreds = ingreds_str.split(' ')
    alergens = alergens_str.split(', ')
    menu.append(Food(set(ingreds), set(alergens)))

known_ingreds_by_alergen = menu.intersections()
safe_ingreds = menu.ingreds - set(ingred for _, ingred in known_ingreds_by_alergen.items())

safe_count = 0
for food in menu.foods:
    for ingred in food.ingreds:
        if ingred in safe_ingreds:
            safe_count += 1
print(safe_count)

print(','.join((known_ingreds_by_alergen[alergen] for alergen in sorted(known_ingreds_by_alergen))))

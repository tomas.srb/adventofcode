#!/usr/bin/env python3

import re

known_fields = ('byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid', 'cid')
mandatory = ('byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid')

data = open('input').read().split('\n\n')

parsed = [dict((field.split(':') for field in record.split())) for record in data]

count_ok = 0

for record in parsed:
    missing = False
    for field in mandatory:
        if field not in record.keys():
            missing = True
    if not missing:
        count_ok += 1

print(count_ok)

regs = {
    'byr': re.compile(r'^\d{4}$'),
    'iyr': re.compile(r'^\d{4}$'),
    'eyr': re.compile(r'^\d{4}$'),
    'hgt': re.compile(r'^\d+(cm|in)$'),
    'hcl': re.compile(r'^#[0-9a-f]{6}$'),
    'ecl': re.compile(r'^(amb|blu|brn|gry|grn|hzl|oth)$'),
    'pid': re.compile(r'^\d{9}$')
}

limits = {
    'byr': (1920, 2002),
    'iyr': (2010, 2020),
    'eyr': (2020, 2030),
}

count_ok = 0
for record in parsed:
    good = True
    for field in mandatory:
        if field not in record.keys():
            good = False
            break
    if good:
        for field, reg in regs.items():
            if not reg.match(record[field]):
                good = False
                break
    if good:
        for field, limit in limits.items():
            val = int(record[field])
            if val < limit[0] or val > limit[1]:
                good = False
                break
    if good:
        height = int(record['hgt'][:-2])
        units = record['hgt'][-2:]
        if units == 'cm' and (height < 150 or height > 193):
            good = False
        if units == 'in' and (height < 59 or height > 76):
            good = False
    if good:
        count_ok += 1

print(count_ok)

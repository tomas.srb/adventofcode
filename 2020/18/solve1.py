#!/usr/bin/env python3

filename = 'input'
#filename = 'test'

def solve(expr):
    total = 0
    par_count = 0
    par_content = ''
    last_oper = '+'
    for ch in expr:
        if par_count == 1 and ch == ')':
            par_count = 0
            right = solve(par_content)
            if last_oper == '+':
                total += right
            elif last_oper == '*':
                total *= right
            else:
                raise Exception('unknown operator %s', last_oper)
            last_oper = None
            continue

        if par_count > 0:
            if ch == ')':
                par_count -= 1
            elif ch == '(':
                par_count += 1
            par_content += ch
            continue

        if ch == '(':
            par_count = 1
            par_content = ''
            continue

        if ch == '+' or ch == '*':
            last_oper = ch

        if ch == ' ':
            continue

        if ch.isnumeric():
            right = int(ch)
            if last_oper == '+':
                total += right
            elif last_oper == '*':
                total *= right
            else:
                raise Exception('unknown operator %s', last_oper)
            last_oper = None
            continue

    print(expr, '=', total) 
    return total

total = sum(solve(line.strip()) for line in open(filename))
print(total)

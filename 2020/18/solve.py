#!/usr/bin/env python3

filename = 'input'
#filename = 'test'

def solve(expr):
    expr = expr.strip()
    if expr.isnumeric():
        return int(expr)

    par_count = 0
    for pos, ch in enumerate(expr):
        if par_count == 0 and ch == '*':
            return solve(expr[:pos]) * solve(expr[pos+1:])
        if ch == '(':
            par_count += 1
        if ch == ')':
            par_count -= 1
    if par_count:
        raise Exception('Nevyvazene zavorky %d v %s', par_count, expr)

    for pos, ch in enumerate(expr):
        if par_count == 0 and ch == '+':
            return solve(expr[:pos]) + solve(expr[pos+1:])
        if ch == '(':
            par_count += 1
        if ch == ')':
            par_count -= 1
    if par_count:
        raise Exception('Nevyvazene zavorky %d v %s', par_count, expr)

    if expr[0] == '(' and expr[-1] == ')':
        return solve(expr[1:-1])
    raise Exception('Nechapu "%s"', expr)

total = sum(solve(line.strip()) for line in open(filename))
print(total)

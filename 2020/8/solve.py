#!/usr/bin/env python3

code = []

for line in open('input'):
    instr, oper = line.strip().split()
    code.append([instr, int(oper), 0])

def run(code, patch):
    cp = 0
    acc = 0
    switch = {
        'acc': 'acc',
        'jmp': 'nop',
        'nop': 'jmp'
    }

    for row in code:
        row[2] = 0

    while True:
        if cp >= len(code):
            print(acc)
            return True
        if code[cp][2] > 0:
            return False
        instr = code[cp][0]
        if cp == patch:
            instr = switch[instr]
        code[cp][2] += 1
        if instr == 'jmp':
            cp += code[cp][1]
            continue
        if instr == 'acc':
            acc += code[cp][1]
            cp += 1
            continue
        if instr == 'nop':
            cp += 1
            continue
        print('Unknown instruction', code[cp])

for patch in range(0, len(code)):
    print(patch, '/', len(code))
    if run(code, patch):
        break

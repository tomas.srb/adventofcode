#!/usr/bin/env python3

filename = 'input'
#filename = 'test'

board = set()
for y, line in enumerate(open(filename).readlines()):
    for x, state in enumerate(line):
        if state == '#':
            board.add((x, y, 0))

def get_cube(point):
    cube = set()
    for x in (-1, 0, 1):
        for y in (-1, 0, 1):
            for z in (-1, 0, 1):
                cube.add((point[0] + x, point[1] + y, point[2] + z))
    return cube

def get_neighs(point):
    neighs = set()
    for x in (-1, 0, 1):
        for y in (-1, 0, 1):
            for z in (-1, 0, 1):
                if x or y or z:
                    neighs.add((point[0] + x, point[1] + y, point[2] + z))
    return neighs

def count_active(board, points):
    return sum(1 for point in points if point in board)

def turn(board):
    solved = set()
    new = set()
    for active_point in board:
        for point in get_cube(active_point):
            if point not in solved:
                active_neigh_count = count_active(board, get_neighs(point))
                if active_neigh_count == 3:
                    new.add(point)
                if active_neigh_count == 2 and point in board:
                    new.add(point)
            solved.add(point)
    return new

for _ in range(6):
    board = turn(board)

print(len(board))

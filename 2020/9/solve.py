#!/usr/bin/env python3

data = [int(x) for x in open('input').read().strip().split('\n')]

if False:
    def check(curr, data):
        for i, a in enumerate(data):
            for j in range(i+1, len(data)):
                if a != data[j] and (a + data[j] == curr):
                    return True
        return False

    for i in range(26, len(data)):
        curr = data[i]
        if not check(curr, data[i-26:i]):
            print(i, curr)
            break

needle = 400480901

for i in range(0, len(data)):
    total = 0
    j = i
    while total < needle:
        total += data[j]
        j += 1
    if total == needle:
        print(i, j, min(data[i:j+1]), max(data[i:j+1]), min(data[i:j+1]) + max(data[i:j+1]))

#!/usr/bin/env python3

import re
from collections import defaultdict

r_tile = re.compile(r'^(Tile (?P<id>\d+):|(?P<line>[#.]+))?\s*$')

filename = 'input'
#filename = 'test1'

class Tile:
    def __init__(self, idn):
        self.idn = idn
        self.lines = []
        self.edges = dict()
        self.coords = tuple()
        self.rot = 0
        self.flip = 0

    def append(self, line):
        self.lines.append(line)

    def prepare(self):
        self.edges = {
            't': self.lines[0],
            'r': ''.join([line[-1] for line in self.lines]),
            'b': self.lines[-1],
            'l': ''.join([line[0] for line in self.lines]),
            'tr': self.lines[0][::-1],
            'rr': ''.join([line[-1] for line in self.lines[::-1]]),
            'br': self.lines[-1][::-1],
            'lr': ''.join([line[0] for line in self.lines[::-1]]),
        }

tiles = []

for line in open(filename):
    m_tile = r_tile.match(line)
    if not m_tile:
        raise Exception('Bad line %s', line)
    if m_tile.group('id'):
        current_tile = Tile(int(m_tile.group('id')))
        tiles.append(current_tile)
    elif m_tile.group('line'):
        current_tile.append(m_tile.group('line'))

for tile in tiles:
    tile.prepare()

edges = defaultdict(list)
for tile in tiles:
    for _, edge in tile.edges.items():
            edges[edge].append(tile)

border_edges = {edge: tiles[0] for edge, tiles in edges.items() if len(tiles) == 1}
border_edges_by_idn = defaultdict(list)
for edge, tile in border_edges.items():
    border_edges_by_idn[tile.idn].append(edge)

total = 1
for idn, edges in border_edges_by_idn.items():
    if len(edges) == 4:
        total *= idn

print(total)


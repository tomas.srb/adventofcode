#!/usr/bin/env python3

import re
from collections import defaultdict
import sys

r_tile = re.compile(r'^(Tile (?P<id>\d+):|(?P<line>[#.]+))?\s*$')

filename = 'input'
#filename = 'test1'

monster = (
    '                  # ',
    '#    ##    ##    ###',
    ' #  #  #  #  #  #   '
)

class Tile:
    def __init__(self, idn):
        self.idn = idn
        self.lines = []
        self.edges = dict()
        self.r_edges = dict()
        self.coords = tuple()
        self.rot = 0
        self.flip = 0

    def append(self, line):
        self.lines.append(line)

    def prepare(self):
        self.edges = {
            't': ''.join(self.lines[0]),
            'r': ''.join([line[-1] for line in self.lines]),
            'b': ''.join(self.lines[-1]),
            'l': ''.join([line[0] for line in self.lines]),
            'tr': ''.join(self.lines[0][::-1]),
            'rr': ''.join([line[-1] for line in self.lines[::-1]]),
            'br': ''.join(self.lines[-1][::-1]),
            'lr': ''.join([line[0] for line in self.lines[::-1]]),
        }
        self.r_edges = {edge: direction for direction, edge in self.edges.items()}

    def place_by_edges(self, edges_l, edges_t):
        print('place_by_edges')
        dirs_l = {self.r_edges[edge] for edge in edges_l if edge in self.r_edges}
        dirs_t = {self.r_edges[edge] for edge in edges_t if edge in self.r_edges}
        print(dirs_l, dirs_t)
        if 't' in dirs_t:
            if 'l' in dirs_l:
                return self.check(edges_l, edges_t)
        if 'tr' in dirs_t:
            if 'r' in dirs_l:
                return self.flip_lr().check(edges_l, edges_t)
        if 'b' in dirs_t:
            if 'lr' in dirs_l:
                return self.flip_tb().check(edges_l, edges_t)
        if 'r' in dirs_t:
            if 'b' in dirs_l:
                return self.rotate(3).check(edges_l, edges_t)
        if 'rr' in dirs_t:
            if 'br' in dirs_l:
                return self.flip_lr().rotate(1).check(edges_l, edges_t)
        self.print()
        print(self.r_edges)
        raise Exception('Invalid transformation edges_l: %s, dirs_l: %s, edges_t: %s, dirs_t: %s', edges_l, dirs_l, edges_t, dirs_t)

    def try_place(self, board, coords):
        around_edges = self.get_around_edges(board, coords)
        checked = self
        for _ in range(3):
            if checked.try_place_direct(board, coords, around_edges):
                return True
            checked = checked.rotate(1)
        checked = checked.flip_lr()
        for _ in range(3):
            if checked.try_place_direct(board, coords, around_edges):
                return True
            checked = checked.rotate(1)
        checked = checked.flip_tb()
        for _ in range(3):
            if checked.try_place_direct(board, coords, around_edges):
                return True
            checked = checked.rotate(1)
        checked = checked.flip_lr()
        for _ in range(3):
            if checked.try_place_direct(board, coords, around_edges):
                return True
            checked = checked.rotate(1)
   
    def try_place_direct(self, board, coords, around_edges): 
            errors = False
#            print('Check:', self.idn, around_edges, self.edges)
            for direction, edge in around_edges.items():
#                print('Side:', direction, 'around: ', edge, 'self:', self.edges[direction], 'border:', self.edges[direction] in border_edges)
                if not (edge == 'ANY' or edge == self.edges[direction] or (edge == 'SIDE' and self.edges[direction] in border_edges)):
                    errors = True
                    break
            if not errors:
                board[coords] = self
                return True

    def get_around_edges(self, board, coords):
        return {
            't': self.get_edge(board, (coords[0], coords[1] - 1), 'b'),
            'r': self.get_edge(board, (coords[0] + 1, coords[1]), 'l'),
            'b': self.get_edge(board, (coords[0], coords[1] + 1), 't'),
            'l': self.get_edge(board, (coords[0] - 1, coords[1]), 'r'),
        }

    def get_edge(self, board, coords, edge):
        if coords in board:
            return board[coords].edges[edge]
        if coords[0] >= 0 and coords[0] < count_x and coords[1] >= 0 and coords[1] < count_y:
            return 'ANY'
        return 'SIDE'

    def check(self, edges_l, edges_t):
        if self.edges['l'] not in edges_l:
            raise Exception('Invalid state edges_l: %s, edges: %s', edges_l, self.edges)
        if self.edges['t'] not in edges_t:
            raise Exception('Invalid state edges_t: %s, edges: %s', edges_t, self.edges)
        return self

    def flip_lr(self):
        print('flip_lr')
        out = Tile(self.idn)
        out.lines = [line[::-1] for line in self.lines]
        out.prepare()
        return out

    def flip_tb(self):
        print('flip_tb')
        out = Tile(self.idn)
        out.lines = [line for line in self.lines[::-1]]
        out.prepare()
        return out

    def rotate(self, n):
        print('rotate')
        out = Tile(self.idn)
        out.lines = list(zip(*self.lines[::-1]))
        if n == 1:
            out.prepare()
            return out
        return out.rotate(n - 1)

    def print(self):
        print(self.idn)
        print('\n'.join([''.join(line) for line in self.lines]))

    def search_monster(self):
        monster = (
            '                  # ',
            '#    ##    ##    ###',
            ' #  #  #  #  #  #   '
        )
        r_monster = [re.compile(line.replace(' ', '.')) for line in monster]
        count = 0
        for x in range(len(self.lines[0]) - len(monster[0])):
            for y in range(len(self.lines) - len(monster)):
                error = False
#                print()
                for dy, r_monster_line in enumerate(r_monster):
#                    print(''.join(self.lines[y + dy][x:x + len(monster[0])]), r_monster_line)
                    if not r_monster_line.match(''.join(self.lines[y + dy][x: x + len(monster[0])])):
                        error = True
                        break
                if not error:
                    count += 1
                    for dy, monster_line in enumerate(monster):
                        print(monster_line)
                        for dx, monster_ch in enumerate(monster_line):
                            if monster_ch == '#':
                                self.lines[y + dy] = self.lines[y + dy][:x + dx] + ('O',) + self.lines[y + dy][x+dx+1:]
                        print(''.join(self.lines[y + dy][x:x + len(monster[0])]))
        return count

tiles = []

for line in open(filename):
    m_tile = r_tile.match(line)
    if not m_tile:
        raise Exception('Bad line %s', line)
    if m_tile.group('id'):
        current_tile = Tile(int(m_tile.group('id')))
        tiles.append(current_tile)
    elif m_tile.group('line'):
        current_tile.append(m_tile.group('line'))

for tile in tiles:
    tile.prepare()

edges = defaultdict(list)
for tile in tiles:
    for _, edge in tile.edges.items():
            edges[edge].append(tile)

border_edges = {edge: tiles[0] for edge, tiles in edges.items() if len(tiles) == 1}
border_edges_by_idn = defaultdict(set)
for edge, tile in border_edges.items():
    border_edges_by_idn[tile.idn].add(edge)

corner_idns = [idn for idn, edges in border_edges_by_idn.items() if len(edges) == 4]
total = 1
for idn in corner_idns:
        total *= idn
print(total)

count_x = int(len(tiles) ** 0.5)
count_y = int(len(tiles) ** 0.5)

count_lines = len(tiles[0].lines)
count_columns = len(tiles[0].lines[0])


if count_x * count_y != len(tiles):
    raise Exception('Board dimension not match')

tiles_by_idns = {tile.idn: tile for tile in tiles}

board = dict()
board_coords = tuple((x, y) for x in range(count_x) for y in range(count_y))

tiles_idn = set(tiles_by_idns.keys())

def try_place(board, tiles_idn, board_coords):
    if not board_coords:
        print('Hotovo!')
        return True
    for idn in tiles_idn:
        if tiles_by_idns[idn].try_place(board, board_coords[0]):
            if try_place(board, tiles_idn - {idn}, board_coords[1:]):
                return True

def get_other_tile(edge, tile, tiles_by_edge):
    candidates = [ntile for ntile in tiles_by_edge[edge] if tile.idn != ntile.idn]
    if not candidates:
        raise Exception('Not found "%s".', edge)
    return candidates[0]

def print_board(board):
    for y in range(count_y):
        for x in range(count_x):
            if (x, y) in board:
                print(board[(x,y)].idn, end=' ')
        print()
        for line in range(count_lines):
            for x in range(count_x):
                if (x, y) in board:
                    print(''.join(board[(x, y)].lines[line]), end=' ')
            print()

def board_to_tile(board):
    tile = Tile(0)
    for y in range(count_y):
        for line in range(1, count_lines - 1):
            line_out = []
            tile.append(line_out)
            for x in range(count_x):
                if (x, y) in board:
                    line_out.extend(board[(x, y)].lines[line][1:-1])
    return tile

try_place(board, tiles_idn, board_coords)
print_board(board)
board_tile = board_to_tile(board)

def search_monster(board_tile):
    checked = board_tile
    for _ in range(3):
        if checked.search_monster():
            return checked
        checked = checked.rotate(1)

    checked = checked.flip_lr()
    for _ in range(3):
        if checked.search_monster():
            return checked
        checked = checked.rotate(1)

    checked = checked.flip_tb()
    for _ in range(3):
        if checked.search_monster():
            return checked
        checked = checked.rotate(1)

    checked = checked.flip_lr()
    for _ in range(3):
        if checked.search_monster():
            return checked
        checked = checked.rotate(1)

board_with_monster = search_monster(board_tile)
count = 0
for line in board_with_monster.lines:
    for ch in line:
        if ch == '#':
            count += 1
print(count)

#
#board = dict()
#board_map = []
#
#count_x = int(len(tiles) ** 0.5)
#count_y = int(len(tiles) ** 0.5)
#
#for x in range(count_x):
#    for y in range(count_y):
#        board_map.append((x, y))
#
#tiles_by_ids = {tile.idn: tile for tile in tiles}
#tiles_ids = set(tiles_by_ids.keys())
#
#def try_place(board, board_map, tiles_ids):
#  if not tiles_ids:
#      return True
#
#  for tile_idn in tiles_ids:
#      for rot in range(4):
#          for flip in range(2):
#              if placed(board, board_map[0], tile_idn, rot, flip):
#                  if try_place(board, board_map[1:], tiles_ids - (tile_idn)
#
#try_place(board, board_map, tiles_ids)
#
#change = True
#while change:
#    change = False
#    for tile in tiles:
#        if tile.coords:
#            for candid in tiles:
#                if not candid.coords:
#                    if tile.match(candid):
#                        change = True
#
#tiles_by_coords = {(tile.coords[0], tile.coords[1]): tile for tile in tiles if tile.coords}
#
#min_x = min(coords[0] for coords in tiles_by_coords)
#min_y = min(coords[1] for coords in tiles_by_coords)
#max_x = max(coords[0] for coords in tiles_by_coords)
#max_y = max(coords[1] for coords in tiles_by_coords)
#
#for y in range(min_y, max_y + 1):
#    for x in range(min_x, max_x + 1):
#        print(tiles_by_coords[(x, y)].idn if (x, y) in tiles_by_coords else 'XXXX', end=' ')
#    print()

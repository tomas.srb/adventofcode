#!/usr/bin/env python3

filename = 'input'
#filename = 'test'

rules = dict()
messages = []

class Const:
    def __init__(self, val):
        self.val = val

    def match(self, sample):
        return {1} if sample[0:1] == self.val else set()

    def __repr__(self):
        return '"'+self.val+'"'

class And:
    def __init__(self, points):
        self.points = points

    def match(self, sample):
        pos = {0}
        for point in self.points:
            new_pos = set()
            for p in pos:
                counts = rules[point].match(sample[p:])
                for count in counts:
                    new_pos.add(count + p)
            pos = new_pos
        return pos

    def __repr__(self):
        return ' '.join(self.points)

class Or:
    def __init__(self, ands):
        self.ands = ands

    def match(self, sample):
        pos = set()
        for item in self.ands:
            counts = item.match(sample)
            pos.update(counts)
        return pos

    def __repr__(self):
        return ' | '.join([repr(item) for item in self.ands])

for line in (line.strip() for line in open(filename).readlines()):
    if not line:
        continue
    if line[0] == 'a' or line[0] == 'b':
        messages.append(line)
        continue
    if line[0] >= '0' and line[0] <= '9':
        parts_colon = line.split(': ')
        if parts_colon[1][0] == '"':
            rules[parts_colon[0]] = Const(parts_colon[1].strip('"'))
        else:
            parts_pipe = parts_colon[1].split(' | ')
            rules[parts_colon[0]] = Or([And(item.split(' ')) for item in parts_pipe])
        continue
    raise Exception('Unknown line %s', line)

print(rules)

print(messages[2])
print(rules['0'].match(messages[2]), len(messages[2]))

matches = 0
for sample in messages:
    if len(sample) in rules['0'].match(sample):
        print('match:', sample)
        matches += 1
    else:
        print('not match:', sample)
print(matches)

#!/usr/bin/env python3

filename = 'input1'
#filename = 'test'

rules = dict()
messages = []

class Const:
    def __init__(self, val):
        self.val = val

    def match(self, sample):
        return 1 if sample[0:1] == self.val else 0

class And:
    def __init__(self, points):
        self.points = points

    def match(self, sample):
        pos = 0
        for point in self.points:
            count = rules[point].match(sample[pos:])
            if not count:
                return 0
            pos += count
        return pos

class Or:
    def __init__(self, ands):
        self.ands = ands

    def match(self, sample):
        for item in self.ands:
            count = item.match(sample)
            if count:
                return count
        return 0

for line in (line.strip() for line in open(filename).readlines()):
    if not line:
        continue
    if line[0] == 'a' or line[0] == 'b':
        messages.append(line)
        continue
    if line[0] >= '0' and line[0] <= '9':
        parts_colon = line.split(': ')
        if parts_colon[1][0] == '"':
            rules[parts_colon[0]] = Const(parts_colon[1].strip('"'))
        else:
            parts_pipe = parts_colon[1].split(' | ')
            rules[parts_colon[0]] = Or([And(item.split(' ')) for item in parts_pipe])
        continue
    raise Exception('Unknown line %s', line)

print(rules)

matches = 0
for sample in messages:
    if rules['0'].match(sample) == len(sample):
        print('match:', sample)
        matches += 1
    else:
        print('not match:', sample)
print(matches)


data = {int(item) for item in open('input').read().split('\n') if item}

result = {(item, 2020 - item, item * (2020 - item)) for item in data if (2020 - item) in data}

print(result)

result = set()

for a in data:
    for b in data:
        if (2020 - a - b) in data:
            result.add((a, b, 2020 - a - b, a * b * (2020 - a - b)))

print(result)

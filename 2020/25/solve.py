#!/usr/bin/env python3

input = (12320657, 9659666)
test = (5764801, 17807724)

pub_keys = input
#pub_keys = test

def process(value, subject_number):
    value = (value * subject_number) % 20201227
    return value

def try_loop(limit):
    value = 1
    subject_number = 7
    end = max(limit)
    i = 0
    counts = [None, None]
    while True:
        i += 1
        value = process(value, subject_number)
        if value in limit:
            index = limit.index(value)
            if counts[index] is None:
                counts[index] = (i, value)
                print(i, value)
            
        if value == end:
            return counts
            

def continue_loop(subject_number, count):
    value = 1
    for i in range(count):
        value = process(value, subject_number)
    return value

loops = try_loop(pub_keys)
secret_key = continue_loop(loops[0][1], loops[1][0])

print(secret_key)

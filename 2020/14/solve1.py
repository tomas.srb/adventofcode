#!/usr/bin/env python3

import re

r_line = re.compile(r'^(mask = (?P<mask>[01X]{36})|mem\[(?P<addr>\d+)\] = (?P<val>\d+))$')

filename = 'input'
#filename = 'test'

memory = dict()
mask = None

def apply_mask(mask, value):
    value_str = '{:036b}'.format(value)
    out = [ch if mask[i] == 'X' else mask[i] for i, ch in enumerate(value_str)]
    print(value_str)
    return int(''.join(out), 2)

for line in open(filename):
    m_line = r_line.match(line)
    if m_line:
        if m_line.group('mask'):
            mask = m_line.group('mask')
        elif m_line.group('addr'):
            memory[m_line.group('addr')] = apply_mask(mask, int(m_line.group('val')))
        else:
            print('bad line:', line)
    else:
        print('bad line:', line)

print(sum([val for _, val in memory.items()]))

#!/usr/bin/env python3

import re

r_line = re.compile(r'^(mask = (?P<mask>[01X]{36})|mem\[(?P<addr>\d+)\] = (?P<val>\d+))$')

filename = 'input'
#filename = 'test2'

memory = dict()
mask = None

def apply_mask(addrs, ch_mask, ch_addr):
    if ch_mask == '0':
        out = [addr + ch_addr for addr in addrs]
    elif ch_mask == '1':
        out = [addr + '1' for addr in addrs]
    elif ch_mask == 'X':
        out = []
        for addr in addrs:
            out.append(addr + '0')
            out.append(addr + '1')
    return out

def write(memory, mask, addr, val):
    addr_str = '{:036b}'.format(addr)
    addrs = ['']
    for i, ch in enumerate(mask):
        addrs = apply_mask(addrs, ch, addr_str[i])
    for addr in addrs:
        memory[addr] = val

for line in open(filename):
    m_line = r_line.match(line)
    if m_line:
        if m_line.group('mask'):
            mask = m_line.group('mask')
        elif m_line.group('addr'):
            write(memory, mask, int(m_line.group('addr')), int(m_line.group('val')))
        else:
            print('bad line:', line)
    else:
        print('bad line:', line)

#print(memory)
print(len(memory))
print(sum([val for _, val in memory.items()]))

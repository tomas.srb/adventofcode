#!/usr/bin/env python3

data = open('input').read().strip().split('\n\n')

counts = [len(set(group.replace('\n', ''))) for group in data]

print(counts)

print(sum(counts))

count = 0

for group in data:
    answers = group.split('\n')
    print(answers)
    uniq = set(answers[0])
    for answer in answers:
        uniq = {ch for ch in uniq if ch in answer}
    print(uniq)
    count += len(uniq)
print(count)

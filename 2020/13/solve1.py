#!/usr/bin/env python3

timestamp = 1004345
bus_ids_str = '41,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,37,x,x,x,x,x,379,x,x,x,x,x,x,x,23,x,x,x,x,13,x,x,x,17,x,x,x,x,x,x,x,x,x,x,x,29,x,557,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,19'.split(',')
bus_ids = [int(bid) for bid in bus_ids_str if bid != 'x']
print(bus_ids)

times_to_wait = [(bid - (timestamp % bid)) for bid in bus_ids]
print(times_to_wait)

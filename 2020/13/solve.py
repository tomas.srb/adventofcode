#!/usr/bin/env python3

from math import gcd

bus_ids_str = '41,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,37,x,x,x,x,x,379,x,x,x,x,x,x,x,23,x,x,x,x,13,x,x,x,17,x,x,x,x,x,x,x,x,x,x,x,29,x,557,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,19'.split(',')
#bus_ids_str = '7,13,x,x,59,x,31,19'.split(',')

bus_ids = [(int(bid), pos) for pos, bid in enumerate(bus_ids_str) if bid != 'x']
print(bus_ids)

def scm(a, b):
    return (a * b) // gcd(a,b)

def check(x, a, offs):
    return (x % a) == ((a - offs) % a)

def search(diff, a, b, offs):
    perioda = scm(a + diff, b)
    print(f'perioda: {perioda}')
    for n in range(0, perioda, a + diff):
        print(f'n: {n}')
        if check(n - diff, b, offs):
            return (perioda - (n - diff), n - diff)

print('-----')
def brute(data):
    print(data)
    for x in range(0, 100000, data[0][0]):
        err = False
        for a, offs in data:
            if not check(x, a, offs):
                err = True
                break
        if not err:
            print(x)

brute(bus_ids[:4])
print('-----')
time = bus_ids[0][0]
diff = 0
for b, offs in bus_ids[1:]:
    diff, time = search(diff, time, b, offs)
    perioda = time + diff
    print(f'{time} + {diff} = {perioda}')

print(f'answer is {time}')
for a, offs in bus_ids:
    print(time % a == (a - offs) % a)

# [(7, 0), (13, 1), (59, 4), (31, 6), (19, 7)]
# 91 - 14 = 77
# 5369 - 3381 = 1988
# 166439 - 8750 = 157689
# 3162341 - 2006018 = 1156323
#                     1068781
#                     


# (7, 13)
# soubeh: 0 0.0 0.0
# reseni: 77 11.0 5.923076923076923
# soubeh: 91 13.0 7.0
# reseni: 168 24.0 12.923076923076923
# soubeh: 182 26.0 14.0
# reseni: 259 37.0 19.923076923076923
# soubeh: 273 39.0 21.0
# reseni: 350 50.0 26.923076923076923
# soubeh: 364 52.0 28.0

# 77 = 7 * 11 = 7 * 5 * 2
# 
# 
# 
# x = 7 * a = 13 * b - 1 = 59 * c - 4 = 31 * d - 6 = 19 * e - 7
# 
# 7 * a - 13 * b = 0
# 
# 
# 0   0
# 7
#    13
# 14
#    26
# 28
#   39
# 35
#   52
# 
# 
# x
#   x + 1

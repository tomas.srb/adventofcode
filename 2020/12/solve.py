#!/usr/bin/env python3

trace = [(line[:1], int(line[1:])) for line in open('input').readlines()]
#trace = [('F', 10), ('N', 3), ('F', 7), ('R', 90), ('F', 11)]

dirs = {
    'N': (-1, 0),
    'S': (1, 0),
    'W': (0, -1),
    'E': (0, 1)
}

angles = {
    0: (1, 0),
    90: (0, 1),
    180: (-1, 0),
    270: (0, -1)
}

rot = {
    'R': 1,
    'L': -1
}

class Pos:
    def __init__(self):
        self.ns = 0
        self.we = 0
        self.wp_ns = -1
        self.wp_we = 10

    def move(self, cmd, count):
        if cmd in dirs:
            self.wp_ns += dirs[cmd][0] * count
            self.wp_we += dirs[cmd][1] * count
        elif cmd in rot:
            we = self.wp_we
            ns = self.wp_ns
            angle = count if cmd == 'R' else 360 - count
            self.wp_we = we * angles[angle][0] - ns * angles[angle][1]
            self.wp_ns = we * angles[angle][1] + ns * angles[angle][0]
        elif cmd == 'F':
            self.ns += self.wp_ns * count
            self.we += self.wp_we * count

    def __str__(self):
        m = self.ns + self.we
        return f'ns: {self.ns}, we: {self.we}, m: {m}, wp_ns: {self.wp_ns}, wp_we: {self.wp_we}'

p = Pos()

for cmd, count in trace:
    p.move(cmd, count)
    print(p)

print(p)

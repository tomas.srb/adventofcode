#!/usr/bin/env python3

trace = [(line[:1], int(line[1:])) for line in open('input').readlines()]
# trace = [('F', 10), ('N', 3), ('F', 7), ('R', 90), ('F', 11)]

ns = 0
we = 0
angle = 0

dirs = {
    'N': (-1, 0),
    'S': (1, 0),
    'W': (0, -1),
    'E': (0, 1)
}

angles = {
    0: dirs['E'],
    90: dirs['S'],
    180: dirs['W'],
    270: dirs['N']
}

rot = {
    'R': 1,
    'L': -1
}

class Pos:
    def __init__(self):
        self.ns = 0
        self.we = 0
        self.angle = 0

    def move(self, cmd, count):
        if cmd in dirs:
            self.ns += dirs[cmd][0] * count
            self.we += dirs[cmd][1] * count
        elif cmd in rot:
            self.angle += rot[cmd] * count
            if self.angle >= 360:
                self.angle -= 360
            if self.angle < 0:
                self.angle += 360
        elif cmd == 'F':
            self.ns += angles[self.angle][0] * count
            self.we += angles[self.angle][1] * count

    def __str__(self):
        m = self.ns + self.we
        return f'ns: {self.ns}, we: {self.we}, angle: {self.angle}, m: {m}'

p = Pos()

for cmd, count in trace:
    p.move(cmd, count)

print(p)
